#!/bin/bash

image="webhook:dev"

iname=$(docker ps | grep $image | awk '{PRINT $1;}')


echo "iname $iname"
if [  "$iname" ]
then
docker stop "$iname"
fi

docker build -t "$image" .

./launchserver.sh
