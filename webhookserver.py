from flask import Flask,request,jsonify,g,current_app,make_response
from datetime import timedelta,datetime
import json
from functools import update_wrapper
app = Flask(__name__)


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

def outputJSON(data,error=[]):
    return jsonify({"data" : data, "error" : error})

returnError = lambda error: outputJSON([],[error])

def setImportantData(jsondata,repo):
    try:
        data = json.loads(jsondata)
    except Exception, e:
        return returnError('could not parse your request as JSON, you sent: ' + str(data)) 
    try:
        pushed = data['push_data']['pushed_at']    
    except:
        return returnError('could not find pushed_at in your request. You sent: ' + str(data))

    try:
        with open('/hostio/'+repo+'.lastpushed', 'w') as f:
            f.write(datetime.fromtimestamp(int(pushed)).strftime('%d-%m-%Y %H:%M:%S'))
    except Exception,e:
        return returnError('could not format or write data: ' + str(e))

    try:
        with open('/hostio/'+repo+'.lastjson', 'w') as f:
            f.write(jsondata)
    except:
        pass

    return outputJSON(['successfully updated entry'])

@app.route('/insert/<repo>', methods=['GET','POST'])
def insertDispatcher(repo):
    error = []
    if not request.args.get('authtok', ''):
        return returnError('Are you sure you are the docker repository?')
    else:
        if request.args.get('authtok','') == 'a228a9fb26822dad15d2dbf81680b6a9':
            return setImportantData(request.data, repo)

@app.route('/get', methods=['GET'])
@crossdomain(origin='*')
def getDispatcher():
    if not request.args.get('repo',''):
        return returnError('repo not specified')
    else:
        repo = request.args.get('repo','')
        try:
            with open('/hostio/'+repo+'.lastpushed','r') as f:
                date = f.read()
                return outputJSON([date])
        except Exception, e:
            return returnError('unable to get date: ' + str(e))

if __name__=='__main__':
    app.run(host='0.0.0.0')
