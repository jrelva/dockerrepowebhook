FROM kkost/uwsgi-docker

ENV HOME /root

RUN rm -f /etc/service/nginx/down /etc/service/uwsgi/down

RUN pip install flask
ADD webhookserver.py /home/app/webapp.py

RUN mkdir /hostio

CMD ["/sbin/my_init"]
